#include <string>
#include <map>
#include <SFML/Window.hpp>
#include <fmod.hpp>

using std::string;
using std::map;
using std::make_pair;

char const * HELLO_SOUND = "data/hello.mp3";


using sound_map = map<string, FMOD::Sound *>;


class simple_audio_manager
{
public:
	simple_audio_manager();
	~simple_audio_manager();
	void load(string const & path);
	void stream(string const & path);
	void play(string const & name);
	void update(float elapsed);

private:
	void load_or_stream(string const & path, bool stream);

	sound_map _sounds;
	FMOD::System * _fmod;
};

simple_audio_manager::simple_audio_manager()
{
	FMOD::System_Create(&_fmod);
	_fmod->init(100, FMOD_INIT_NORMAL, nullptr);
}

simple_audio_manager::~simple_audio_manager()
{
	for (sound_map::iterator it = _sounds.begin(); it != _sounds.end(); ++it)
		it->second->release();
	_sounds.clear();

	_fmod->release();
	_fmod = nullptr;
}

void simple_audio_manager::update(float elapsed)
{
	_fmod->update();
}

void simple_audio_manager::load_or_stream(string const & path, bool stream)
{
	if (_sounds.find(path) != _sounds.end())
		return;  // sound already loaded

	// load sound
	FMOD::Sound * sound;
	if (stream)
		_fmod->createStream(path.c_str(), FMOD_DEFAULT, 0, &sound);
	else
		_fmod->createSound(path.c_str(), FMOD_DEFAULT, 0, &sound);

	_sounds.insert(make_pair(path, sound));
}

void simple_audio_manager::load(string const & path)
{
	load_or_stream(path, false);
}

void simple_audio_manager::stream(string const & path)
{
	load_or_stream(path, true);
}

void simple_audio_manager::play(string const & name)
{
	sound_map::iterator it = _sounds.find(name);
	if (it == _sounds.end())
		return;

	_fmod->playSound(it->second, nullptr, false, nullptr);
}


int main(int argc, char * argv[])
{
	sf::Clock clock;

	simple_audio_manager a;
	a.load(HELLO_SOUND);

	sf::Window w{sf::VideoMode{320, 240}, "Audio Playback"};
	while (w.isOpen())
	{
		float elapsed = clock.getElapsedTime().asSeconds();
		if (elapsed < 1.0f/60.0f)
			continue;

		clock.restart();

		sf::Event e;
		while (w.pollEvent(e))
		{
			// window events
			if (e.type == sf::Event::Closed)
				w.close();

			// user input
			if (e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Space)
				a.play(HELLO_SOUND);
		}

		a.update(elapsed);
	}

	return 0;
}
